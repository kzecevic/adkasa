<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\AdminPageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\InsertController;
use App\Http\Controllers\FailedResultsController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\ScalesController;
use App\Http\Controllers\narudzbe\NarudzbeController;
use App\Http\Controllers\narudzbe\AromaLogin;
use Illuminate\Support\Facades\Route;




//---------------------INERTIA-----------------

//prava (samo vue), request i response su u starim laravel rutama
Route::get('/', function () {
    return inertia('Prava');
})->middleware("auth");

//aroma narudzbe
Route::get('/aroma/narudzbe', [NarudzbeController::class, 'GetAllNarudzbe'])->middleware("beris.auth");
Route::put('/aroma/novanarudzba', [NarudzbeController::class, 'NewNarudzba'])->middleware("beris.auth");
Route::post('/aroma/narudzba/edit', [NarudzbeController::class, 'EditNarudzba'])->middleware("beris.auth");
Route::post('/aroma/proizvodnja/edit', [NarudzbeController::class, 'EditProizvodnja'])->middleware("beris.auth");
Route::post('/aroma/getAllData', [NarudzbeController::class, 'GetAllData'])->middleware("beris.auth");
Route::post('/aroma/stavka/get', [NarudzbeController::class, 'GetStavka'])->middleware("beris.auth");
Route::post('/aroma/stavka/new', [NarudzbeController::class, 'NovaStavka'])->middleware("beris.auth");
Route::post('/aroma/stavka/edit', [NarudzbeController::class, 'EditStavka'])->middleware("beris.auth");
Route::get('/aroma/login', function () {
    return view('components.aromaLogin');
});
Route::post('/aroma/login/auth', [AromaLogin::class, 'Login']);

//vage (sve)
Route::get('/chart', [ScalesController::class, 'GetData'])->middleware("beris.auth");
Route::put('/updateFridgePots', [ScalesController::class, 'UpdateFridgePots'])->middleware("beris.auth");




//All non-inertia routes are depricated and should not be used in any cases except login !!!!!!!!!(19.01.2022)!!!!!!!!

//Login routes----------------------------------------------------------------------
Route::post('/login/auth', [LoginController::class, 'Login']);
Route::get('/logout', [LoginController::class, 'Logout']);

//User routes----------------------------------------------------------------------
Route::post('/addUser', [UserController::class, 'Add']);
Route::get('/getUsers', [UserController::class, 'GetUsers']);
Route::post('/editUser', [UserController::class, 'EditUser']);
Route::post('/newPassword', [UserController::class, 'NewPassword']);

//Role routes----------------------------------------------------------------------
Route::post('/userRoles', [RolesController::class, 'GetRoles']);
Route::post('/updateRoles', [RolesController::class, 'UpdateRoles']);
Route::post('/addRole', [RolesController::class, 'AddRole']);

//login
Route::get('/login', function () {
    return view('components.login');
});


//-------------------------------------------------------------------
//--------------------------SALESFORCE-------------------------------

Route::get('/salesforce/insert', [InsertController::class, 'Insert']);
Route::get('/salesforce/klijenti', function () {
    return view('components.salesforce-transfer-ui');
});
Route::get('/salesforce/racuni', function () {
    return view('components.salesforce-transfer-ui-racuni');
});

Route::get('/salesforce/getFailedUsers', [FailedResultsController::class, 'GetFailedUsers']);
Route::get('/salesforce/getFailedCases', [FailedResultsController::class, 'GetFailedCases']);
Route::get('/salesforce/insertbulk', [InsertController::class, 'InsertBulk']);
Route::get('/salesforce/insertsingleuser', [AccountController::class, 'InsertSingle']);
