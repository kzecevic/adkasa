new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data: () => ({
        valid: true,
        name: "",
        nameRules: [(v) => !!v || "Morate unjeti korisničko ime"],
        password: "",
        passwordRules: [(v) => !!v || "Morate unjeti šifru!"],
        alertShow: false,
        alertMessage: "poruka",
        alertType: "success",
    }),

    methods: {
        validate() {
            if (this.$refs.form.validate()) {
                axios
                    .post("/aroma/login/auth", {
                        username: this.name,
                        password: this.password,
                    })
                    .then((response) => {
                        console.log(response);
                        if (response.status === 200)
                            window.location.replace("/aroma/narudzbe")
                        else if (response.status === 406) {
                            this.showAlert(response.data, "error")
                        }
                    })
                    .catch((error) => {
                        this.showAlert(error.response.data, "error")
                        console.log(error);
                    });
            }
        },

        showAlert(message, type) {
            this.alertMessage = message;
            this.alertType = type;
            this.alertShow = true;
            setTimeout(() => {
                this.alertShow = false;
            }, 3000);
        },
    },
});
