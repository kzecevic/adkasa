new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data: () => ({
        height: 1000,
        alertShow: false,
        alertMessage: "poruka",
        alertType: "success",
        newCheckbox: "",
        loading: false,
        search: "",
        dark: false,
        dense: false,
        fab: true,
        singleSelect: false,
        expanded: [],
        singleExpand: true,
        expanded: [],
        selected: [],
        headers: [
            {
                text: "Klijent",
                align: "start",
                value: "UserID",
                width: 200,
                divider: true,
                filterable: true,
            },
            {
                text: "Izvor",
                align: "start",
                value: "RacBranch",
                width: 200,
                divider: true,
                filterable: true,
            },
            {
                text: "ID računa",
                value: "RacID",
                width: 150,
                divider: true,
                sortable: false,
                filterable: false,
            },
            {
                text: "Iznos 1",
                value: "RacAmount",
                width: 200,
                divider: true,
                sortable: false,
                filterable: false,
            },
            {
                text: "iznos 2",
                value: "RacAmount2",
                width: 200,
                divider: true,
                sortable: false,
                filterable: false,
            },
            {
                text: "",
                value: "actions",
                sortable: false,
                width: 40,
                divider: false,
                filterable: false,
            },
            { text: "", value: "data-table-expand", width: 10, divider: false },
        ],
        failedResults: [],
        editedItem: {
            OIB: "",
            Ime: "",
            Prezime: "",
            ID: "",
            Izvor: "",
            TransferLog: -1,
        },
        // defaultItem: {
        //     username: "",
        //     imeRacun: "",
        //     password: "",
        //     idUsertest: -1,
        //     IdOperateri: -1,
        //     lastLogin: "",
        //     roles: [],
        //     // OIB: '',
        //     // username: '',
        //     // password:'',
        //     // imeRacun: '',
        //     // lastLogin: '',
        // },
    }),

    computed: {
        formTitle() {
            return this.editedIndex === -1 ? "New Item" : "Edit Item";
        },
    },

    watch: {
        dialog(val) {
            val || this.close();
        },
    },

    created() {
        this.initialize();
    },

    methods: {
        initialize() {
            this.loading = true;
            axios
                .get("/salesforce/getFailedCases")
                .then((response) => {
                    this.failedResults = response.data;
                    this.loading = false;
                })
                .catch(function (error) {
                    console.log(error);
                });
            this.dark = localStorage.getItem("dark") == "true" ? true : false;
            this.dense = localStorage.getItem("dense") == "true" ? true : false;
        },

        pushOne(item) {
            this.loading = true;
            Object.assign(this.editedItem, item);

            axios
                .post("/salesforce/pushOne", {
                    ID: item.ID,
                })
                .then((response) => {
                    this.refreshFailedResults();
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        refreshFailedResults() {
            this.loading = true;
            axios
                .get("/salesforce/getFailedCasesS")
                .then((response) => {
                    this.failedResults = response.data;
                    this.loading = false;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        // close() {
        //     this.dialog = false;
        //     this.dialogRoles = false;
        //     this.dialogAddRole = false;
        //     this.dialogEditUser = false;
        //     this.$nextTick(() => {
        //         this.editedItem = Object.assign({}, this.defaultItem);
        //         this.editedIndex = -1;
        //     });
        //     this.refreshUsers();
        // },
        // closeRoles() {
        //     this.close();
        // },

        showAlert(message, type) {
            this.alertMessage = message;
            this.alertType = type;
            this.alertShow = true;
            setTimeout(() => {
                this.alertShow = false;
            }, 3000);
        },

        darkDenseChanged() {
            console.log("sadsad");
            localStorage.setItem("dark", this.dark);
            localStorage.setItem("dense", this.dense);
        },

        rowClick(item, slot) {
            console.log("row clicked");
        },
    },
});
