new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data: () => ({
        valid: true,
        name: "",
        nameRules: [(v) => !!v || "Morate unjeti korisničko ime"],
        password: "",
        passwordRules: [(v) => !!v || "Morate unjeti šifru!"],
        alertShow: false,
        alertMessage: "poruka",
        alertType: "success",
    }),

    methods: {
        validate() {
            if (this.$refs.form.validate()) {
                axios
                    .post("/login/auth", {
                        username: this.name,
                        password: this.password,
                    })
                    .then((response) => {
                        console.log(response);
                        if (response.status === 200)
                            window.location.replace("/")
                        else if(response.status === 204){
                            this.showAlert("Korisnik nema admin prava", "error")
                        }
                        else
                            this.showAlert("Neispravni podaci", "error")
                    })
                    .catch((error) => {
                        this.showAlert("Neispravni podaci", "error")
                        console.log(error);
                    });
            }
        },

        showAlert(message, type) {
            this.alertMessage = message;
            this.alertType = type;
            this.alertShow = true;
            setTimeout(() => {
                this.alertShow = false;
            }, 3000);
        },
    },
});
