new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data: () => ({
        height: 1000,
        dialogRoles: false,
        dialogAddRole: false,
        dialog: false,
        dialogEditUser: false,
        alertShow: false,
        alertMessage: "poruka",
        alertType: "success",
        newCheckbox: "",
        newRole: "",
        tmpIdUserTest: "",
        loading: false,
        tmpPassword: "",
        search: "",
        dark: false,
        dense: false,
        fab: true,
        expanded: [],
        headers: [
            {
                text: "Ime i prezime",
                align: "start",
                value: "username",
                width: 200,
                divider: true,
                filterable: true,
            },
            {
                text: "Šifra",
                value: "fakePassword",
                width: 150,
                divider: true,
                sortable: false,
                filterable: false,
            },
            {
                text: "Ime na računu",
                value: "imeRacun",
                width: 150,
                divider: true,
                sortable: false,
                filterable: false,
            },
            {
                text: "OIB",
                value: "OIB",
                width: 200,
                divider: true,
                sortable: false,
                filterable: false,
            },
            {
                text: "Operater ID",
                value: "IdOperateri",
                width: 200,
                divider: true,
                sortable: false,
                filterable: false,
            },
            {
                text: "User ID",
                value: "idUsertest",
                width: 200,
                divider: true,
                sortable: false,
                filterable: false,
            },
            {
                text: "Zadnji login",
                value: "lastLogin",
                width: 200,
                divider: true,
                filterable: false,
                sort: (a, b) => {
                    first = a.split("/");
                    second = b.split("/");
                    a = new Date(first[2], first[1], first[0]);
                    b = new Date(second[2], second[1], second[0]);
                    return a - b;
                },
            },
            {
                text: "",
                value: "actions",
                sortable: false,
                width: 200,
                divider: true,
                filterable: false,
            },
        ],
        users: [],
        editedItem: {
            OIB: "",
            username: "",
            imeRacun: "",
            password: "",
            fakePassword: "",
            idUsertest: -1,
            IdOperateri: -1,
            lastLogin: "",
            roles: [],
        },
        // defaultItem: {
        //     username: "",
        //     imeRacun: "",
        //     password: "",
        //     idUsertest: -1,
        //     IdOperateri: -1,
        //     lastLogin: "",
        //     roles: [],
        //     // OIB: '',
        //     // username: '',
        //     // password:'',
        //     // imeRacun: '',
        //     // lastLogin: '',
        // },
    }),

    computed: {
        formTitle() {
            return this.editedIndex === -1 ? "New Item" : "Edit Item";
        },
    },

    watch: {
        dialog(val) {
            val || this.close();
        },
        dialogDelete(val) {
            val || this.closeDelete();
        },
        dialogRoles(val) {
            val || this.closeRoles();
        },
        dialogEditUser(val) {
            val || this.close();
        },
    },

    created() {
        this.initialize();
    },

    methods: {
        initialize() {
            this.loading = true;
            axios
                .get("/getUsers")
                .then((response) => {
                    response.data.forEach((element) => {
                        element.fakePassword = "********";
                    });
                    this.users = response.data;
                    this.loading = false;
                })
                .catch(function (error) {
                    console.log(error);
                });
            this.dark = localStorage.getItem("dark") == "true" ? true : false;
            this.dense = localStorage.getItem("dense") == "true" ? true : false;
        },

        refreshUsers() {
            this.loading = true;
            axios
                .get("/getUsers")
                .then((response) => {
                    response.data.forEach((element) => {
                        element.fakePassword = "********";
                    });
                    this.users = response.data;
                    this.loading = false;
                    console.log("reset");
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        editRoles(item) {
            this.loading = true;
            Object.assign(this.editedItem, item);

            axios
                .post("/userRoles", {
                    userId: item.idUsertest,
                })
                .then((response) => {
                    this.editedItem.roles = response.data;
                    this.$nextTick(() => {
                        this.dialogRoles = true;
                        this.loading = false;
                    });
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        editRolesAfterUserAdd() {
            this.loading = true;
            axios
                .post("/userRoles", {
                    userId: this.tmpIdUserTest,
                })
                .then((response) => {
                    this.editedItem.roles = response.data;
                    this.dialogRoles = true;
                    this.editedItem.idUsertest = this.tmpIdUserTest;
                    this.loading = false;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        close() {
            this.dialog = false;
            this.dialogRoles = false;
            this.dialogAddRole = false;
            this.dialogEditUser = false;
            this.$nextTick(() => {
                this.editedItem = Object.assign({}, this.defaultItem);
                this.editedIndex = -1;
            });
            this.refreshUsers();
        },
        closeRoles() {
            this.close();
        },

        showAlert(message, type) {
            this.alertMessage = message;
            this.alertType = type;
            this.alertShow = true;
            setTimeout(() => {
                this.alertShow = false;
            }, 3000);
        },

        openAddRoles() {
            this.dialogAddRole = true;
        },
        closeAddRoles() {
            this.dialogAddRole = false;
        },

        openAddUser() {
            this.dialog = true;
        },

        //---------------------------------------Save functions---------------------------------------------

        save() {
            this.loading = true;
            axios
                .post("/addUser", {
                    username: this.editedItem.username,
                    OIB: this.editedItem.OIB,
                    imeRacun: this.editedItem.imeRacun,
                })
                .then((response) => {
                    if (response.status == 200) {
                        this.showAlert(response.data[0], "success");
                        this.close();
                        this.refreshUsers();
                        this.tmpIdUserTest = response.data[1];
                        this.$nextTick(() => {
                            this.editRolesAfterUserAdd();
                            this.loading = false;
                        });
                    } else if (response.status == 204) {
                        this.showAlert("Morate unjeti ime korisnika", "error");
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    this.refreshUsers();
                });
        },
        saveRoles() {
            this.loading = true;
            axios
                .post("/updateRoles", {
                    roles: this.editedItem.roles,
                    userId: this.editedItem.idUsertest,
                })
                .then((response) => {
                    console.log(response);
                    this.showAlert(response.data, "success");
                    this.close();
                    this.loading = false;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        saveAddRoles() {
            this.loading = true;
            axios
                .post("/addRole", {
                    roleName: this.newRole,
                })
                .then((response) => {
                    if (response.status == 200) {
                        console.log(response);
                        this.showAlert(response.data, "success");
                        this.close();
                        this.loading = false;
                    } else if (response.status == 204) {
                        this.showAlert(
                            "Morate unjeti ime uloge/prava",
                            "error"
                        );
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        closeEditUser(item) {
            this.close();
        },
        saveEditUser(item) {
            this.loading = true;
            axios
                .post("/editUser", {
                    idUsertest: this.editedItem.idUsertest,
                    idOperater: this.editedItem.IdOperateri,
                    OIB: this.editedItem.OIB,
                    imeRacun: this.editedItem.imeRacun,
                    username: this.editedItem.username,
                })
                .then((response) => {
                    console.log(response);
                    this.refreshUsers();
                    this.close();
                    this.showAlert("Korisnički podaci izmjenjeni", "success");
                    this.loading = false;
                })
                .catch(function (error) {
                    this.showAlert(error, "error");
                    console.log(error);
                    this.refreshUsers();
                });
        },
        editUser(item) {
            this.editedItem = item;
            this.tmpPassword = item.password;
            this.dialogEditUser = true;
        },

        generateNewPassword() {
            this.loading = true;
            axios
                .post("/newPassword", {
                    idOperater: this.editedItem.IdOperateri,
                    idUsertest: this.editedItem.idUsertest,
                })
                .then((response) => {
                    if (response.status == 200) {
                        this.showAlert(
                            "Šifra izmjenjena: " + response.data.Sifra,
                            "success"
                        );
                        this.tmpPassword = response.data.Sifra;
                        this.refreshUsers();
                        this.loading = false;
                    } else if (response.status == 204) {
                        this.showAlert("Server error", "error");
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    this.refreshUsers();
                });
        },
        logout() {
            this.loading = true;
            axios
                .get("/logout")
                .then((response) => {
                    location.reload();
                })
                .catch(function (error) {
                    location.reload();
                });
        },
        darkDenseChanged() {
            console.log("sadsad");
            localStorage.setItem("dark", this.dark);
            localStorage.setItem("dense", this.dense);
        },
        // onScroll(e) {
        //     if (typeof window === "undefined") return;
        //     const top = window.pageYOffset || e.target.scrollTop || 0;
        //     this.fab = top > 20;
        // },
        toTop() {
            this.$vuetify.goTo(0);
        },

        rowClick(item, slot) {
            item.fakePassword =
                item.password == item.fakePassword ? "********" : item.password;
        },

    },
});
