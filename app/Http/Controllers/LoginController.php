<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Facade\FlareClient\Http\Response;
use Illuminate\Database\Console\Migrations\StatusCommand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Environment\Console;
use Illuminate\Support\Carbon;
use Symfony\Component\Console\Input\Input;

class LoginController extends Controller
{
    // public function validation(Request $request){
    //     $validated = $request->validate([
    //         'username' => 'required|max:255',
    //         'password' => 'required',
    //     ]);
    // }

    public function Login(Request $request)
    {
        // $ID_usertest = DB::select('select ID_usertest from usertest where upper(ImePrezime) = :username and Sifra = :password',
        //     ['username' => $request->input('username'), 'password' => $request->input('password')]);

        try {
            $input = $request->input();
            error_log(json_encode($input));


            $userId = DB::table('usertest')->where([
                ['ImePrezime', $request->input('username')],
                ['Sifra', $request->input('password')],
            ])->pluck('ID_usertest');

            error_log($userId[0]);
            $userId = $userId[0];

            if (!isset($userId)) {
                return response('1st invalid username or password', $status = 204);
            } else {
                if (DB::table('Prava')->where([
                    ['IDOdjelMe', 37],
                    ['Ulaz', 1],
                    ['IDUserTest', $userId]
                ])->exists()) {
                    //return response('inasdasdasdd', $status = 404);
                    error_log('authorized');
                    $request->session()->put('authorized', true);
                    $request->session()->put('userId', $userId);
                    $request->session()->put('username', $request->input('username'));
                    DB::table('Usertest')->where('ID_usertest', $userId)->update(['DatumZadnjegLogina' => Carbon::now()->toDateTimeString()]);
                    return response('sucess', 200);
                } else {
                    error_log('nema prava');
                    return response('notadmin', $status = 204);
                }
            }
        } catch (\Exception $e) {
            error_log('exception: ' . $e);
            return response('invalid username or password', $status = 404);
        }
    }

    public function Logout(Request $req)
    {
        try {
            error_log('logging out');
            $req.session()->flush();

        } catch (\Exception $e) {
            error_log('exception: ' . $e);
            return response('not logged out', $status = 204);
        }
    }
}
