<?php

namespace App\Http\Controllers\narudzbe;

use App\Models\Narudzbe;
use App\Models\Stavke;
use App\Models\Kupci;
use App\Models\AromaUser;
use App\Http\Controllers\Controller;
use App\Models\Roba;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use PhpParser\Error as PhpParserError;

use function Symfony\Component\String\b;

class NarudzbeController extends Controller
{

    public function GetAllNarudzbe(Request $req)
    {
        try {
            $input = $req->collect();
            if (!isset($input["first"]) || $input["first"] == "true") {
                error_log("first req; data: " . json_encode($input));
                return inertia('aroma/Narudzbe', [

                    'narudzbe' => $req->session()->get('Prava') == "Admin" ? Narudzbe::with('kupac')->get() : Narudzbe::with('kupac')->where("IDWEBKUP", $req->session()->get('IDWEBKUP'))->get(),
                    'user' => AromaUser::where('ID', $req->session()->get('userId'))->first()

                ]);
            } else {
                error_log("NON 1st req,data: " . json_encode($input));
                // $test = IcecreamPot::where('fridge_id', $input["fridge_id"])->get();
                // error_log(json_encode($test));
                // $test = isset($input["fridge_id"]) ? IcecreamPot::where("fridge_id", $input["fridge_id"])->get(): null;
                // error_log(json_encode($test));
                // $datetime = isset($input["datetime"]) ? $input["datetime"] : Carbon::now();
                return inertia('aroma/Narudzbe', [
                    'narudzbe' => $req->session()->get('Prava') == "Admin" ? Narudzbe::with('kupac')->get() : Narudzbe::with('kupac')->where("IDWEBKUP", $req->session()->get('IDWEBKUP'))->get(),
                    'newID' => NarudzbeController::NewNarudzba(),
                    'user' => AromaUser::where('ID', $req->session()->get('userId'))->first()
                ]);
            }
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }

    public static function NewNarudzba()
    {
        try {
            $nova = new Narudzbe;
            $nova->save();
            error_log($nova->id);
            return $nova->id;
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
    public static function EditNarudzba(Request $req)
    {
        try {
            $input = json_decode($req->collect());
            error_log("edit narudzbe: " . json_encode($input->narudzba));
            error_log("test value: " . json_encode($input->narudzba->Nadnevak_Dostave));

            $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->first();
            if ($req->session()->get('Prava') == "Admin") {
                $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->update([
                    'IDWEBKUP' => $input->narudzba->IDWEBKUP,
                    'NadnevakZaDostavu' => $input->narudzba->NadnevakZaDostavu,
                    'NapomeneNarudzbe' => $input->narudzba->NapomeneNarudzbe,
                    'Nadnevak_Dostave' => $input->narudzba->Nadnevak_Dostave,
                    'NapomeneDostava' => $input->narudzba->NapomeneDostava,
                    // 'Nadnevak' => $input->narudzba->Nadnevak,
                    'Zakljucano' => $input->narudzba->Zakljucano,
                    'NadnevakZakljucano' => $input->narudzba->Zakljucano == 1 ? Carbon::now() : null
                ]);
            } else {
                $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->first();
                if (($narudzba->Zakljucano == 0 && $input->narudzba->Zakljucano == 1)) {
                    $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->update([
                        'IDWEBKUP' => $req->session()->get('IDWEBKUP'),
                        'NadnevakZaDostavu' => $input->narudzba->NadnevakZaDostavu,
                        'NapomeneNarudzbe' => $input->narudzba->NapomeneNarudzbe,
                        'Nadnevak_Dostave' => $input->narudzba->Nadnevak_Dostave,
                        'NapomeneDostava' => $input->narudzba->NapomeneDostava,
                        // 'Nadnevak' => $input->narudzba->Nadnevak,
                        'Zakljucano' => $input->narudzba->Zakljucano,
                        'NadnevakZakljucano' => Carbon::now()
                    ]);
                } else {
                    $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->update([
                        'IDWEBKUP' => $req->session()->get('IDWEBKUP'),
                        'NadnevakZaDostavu' => $input->narudzba->NadnevakZaDostavu,
                        'NapomeneNarudzbe' => $input->narudzba->NapomeneNarudzbe,
                        'Nadnevak_Dostave' => $input->narudzba->Nadnevak_Dostave,
                        'NapomeneDostava' => $input->narudzba->NapomeneDostava,
                        // 'Nadnevak' => $input->narudzba->Nadnevak,
                    ]);
                }
            }

            $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->first();
            error_log(json_encode($narudzba));
            return response(['narudzba' => $narudzba]);
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
    public static function EditProizvodnja(Request $req)
    {
        try {
            $input = json_decode($req->collect());
            error_log("edit proizvodnja narudzbe: " . json_encode($input->narudzba));
            $narudzba = null;

            if ($req->session()->get('Prava') == "Admin") {
                $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->first();
                if (($narudzba->ZakljucanoProizvodnja == 0 && $input->narudzba->ZakljucanoProizvodnja == 1)) {
                    $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->update([
                        'NadnevakProizvodnje' => $input->narudzba->NadnevakProizvodnje,
                        'NapomeneProizvodnja' => $input->narudzba->NapomeneProizvodnja,
                        'ZakljucanoProizvodnja' => $input->narudzba->ZakljucanoProizvodnja,
                        'NadnevakZakljucanoProizvodnja' => Carbon::now()
                    ]);
                } else {
                    $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->update([
                        'NadnevakProizvodnje' => $input->narudzba->NadnevakProizvodnje,
                        'NapomeneProizvodnja' => $input->narudzba->NapomeneProizvodnja,
                        'ZakljucanoProizvodnja' => $input->narudzba->ZakljucanoProizvodnja,
                    ]);
                }
            }

            $narudzba = Narudzbe::where('ID', $input->narudzba->ID)->first();
            error_log(json_encode($narudzba));
            return response(['narudzba' => $narudzba]);
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
    public static function GetAllData(Request $req)
    {
        try {
            $input = $req->collect();
            error_log("getalldata " . $input['id']);
            $narudzba = Narudzbe::where('ID', $input['id'])->get();
            error_log(json_encode($narudzba));
            $stavke = Stavke::with('roba')->where('IDWEB_narudzbe', $input['id'])->get();
            error_log('asdasd  ' . json_encode($stavke));
            $kupci = Kupci::all();
            return response(['narudzba' => $narudzba[0], 'stavke' => $stavke, 'kupci' => $kupci]);
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }

    public static function GetStavka(Request $req)
    {
        try {
            $input = $req->collect();
            error_log("get stavka " . $input['id']);
            $stavka = Stavke::where('ID', $input['id'])->get();
            $robe = Roba::all();
            return response(['stavka' => $stavka[0], 'robe' => $robe]);
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
    public static function NovaStavka(Request $req)
    {
        try {
            $input = $req->collect();
            error_log("nova stavka za narudzbu: " . $input['idNarudzbe']);
            $stavka = Stavke::create([
                'IDWEB_narudzbe' => $input['idNarudzbe'],
            ]);
            return response(['stavka' => $stavka]);
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
    public static function EditStavka(Request $req)
    {
        try {
            $input = json_decode($req->collect());
            error_log("edit stavke: " . $input->stavka->ID);
            Stavke::where('ID', $input->stavka->ID)->update(
                [
                    'IDWEB_ROBE' => $input->stavka->IDWEB_ROBE,
                    'OPISWEB_ROBE' => $input->stavka->OPISWEB_ROBE,
                    'Kolicina' => $input->stavka->Kolicina,
                    'CijenaBezPdv' => $input->stavka->CijenaBezPdv,
                    'CijenaSaPdv' => $input->stavka->CijenaSaPdv,
                    'Tezina' => $input->stavka->Tezina,
                    'Napomene' => $input->stavka->Napomene
                ]
            );
            $stavka = Stavke::where('ID', $input->stavka->ID)->first();
            error_log('test stavka    ' . json_encode($stavka->IDWEB_narudzbe));
            Narudzbe::where('ID', $stavka->IDWEB_narudzbe)
                ->update(['UkupnaKolicina' => Stavke::where('IDWEB_narudzbe', $stavka->IDWEB_narudzbe)->sum('Kolicina')]);
            error_log('edited: ' . json_encode($stavka));
            return response(["stavka" => $stavka]);
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
}
