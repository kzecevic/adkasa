<?php

namespace App\Http\Controllers\narudzbe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class AromaLogin extends Controller
{
    public function Login(Request $request)
    {
        // $ID_usertest = DB::select('select ID_usertest from usertest where upper(ImePrezime) = :username and Sifra = :password',
        //     ['username' => $request->input('username'), 'password' => $request->input('password')]);

        try {
            $input = $request->input();
            error_log(json_encode($input));


            $user = DB::connection('aroma')->table('WEB_user')->where([
                ['Korisnickoime', $request->input('username')],
                ['SifraWEB', $request->input('password')],
            ])->get();

            error_log(json_encode($user));

            if (!isset($user[0])) {
                return response('Kriva sifra ili korisnicko ime', $status = 406);
            } else {

                $user = $user[0];
                error_log($user->Aktivan);
                if ($user->Aktivan != 1) return response('Korisnik neaktivan', $status = 406);
                if ($user->PravaWEB == "Admin") {
                    error_log('admin authorized');
                    $request->session()->put('authorizedAroma', true);
                    $request->session()->put('userId', $user->ID);
                    $request->session()->put('KorisnickoIme', $request->input('username'));
                    $request->session()->put('Prava', 'Admin');
                    return response('sucess', 200);
                }
                if ($user->PravaWEB == "Operater") {
                    error_log('operater authorized');
                    $request->session()->put('authorizedAroma', true);
                    $request->session()->put('userId', $user->ID);
                    $request->session()->put('KorisnickoIme', $request->input('username'));
                    $request->session()->put('IDWEBKUP', $user->IDWEB_KUP);
                    $request->session()->put('Prava', 'Operater');
                    error_log('sending 200');
                    return response('sucess', 200);
                }
            }
        } catch (\Exception $e) {
            error_log('exception: ' . $e);
            return response('exception caught! maybe invalid username or password', $status = 406);
        }
    }

    public function Logout(Request $req)
    {
        try {
            error_log('logging out');
            $req . session()->flush();
        } catch (\Exception $e) {
            error_log('exception: ' . $e);
            return response('not logged out', $status = 204);
        }
    }
}
