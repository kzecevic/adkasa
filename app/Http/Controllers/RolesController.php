<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    function UpdateRoles(Request $req)
    {
        try {
            if ($req->session()->get('authorized', false)) {

                $userId = $req->input('userId');
                $roles = $req->input('roles');

                foreach ($roles as $role) {
                    if (isset($role['userId'])) {
                        if ($role['userId'] == 'true') {
                            if (!DB::table('Prava')->where([['IDUsertest', $userId], ['IDOdjelMe', $role['roleId']]])->exists()) {
                                error_log('adding');
                                DB::table('Prava')->insert([
                                    'IDUsertest' => $userId,
                                    'IDOdjelMe' => $role['roleId'],
                                    'Ulaz' => 1
                                ]);
                            }
                        }
                        if ($role['userId'] == false) {
                            error_log('deleting');
                            DB::table('Prava')->where([['IDUsertest', $userId], ['IDOdjelMe', $role['roleId']]])->delete();
                        }
                    }
                }
                return response('Prava/uloge izmjenjene', 200);
            }
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }

    function AddRole(Request $req)
    {
        try {
            if ($req->session()->get('authorized', false)) {

                if ($req->input('roleName') == '')
                    return response('Morate unjeti ime uloge/prava', 204);

                $newRole = $req->input('roleName');
                $newRoleId = DB::table('OdjeliMe')->insertGetId(['OdjelMe' => $newRole]);
                return response('Dodana nova uloga/pravo sa ID: ' . $newRoleId);
            }
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }

    public static function GetRoles(Request $req)
    {
        try {
            error_log('uslo u roles ctrl');
            if ($req->session()->get('authorized', false)) {

                $userId = $req->input('userId');
                $roles = DB::select('SELECT ID_OdjeliMe as roleId, OdjelMe as roleName, Prava.IDUsertest as userId, Prava.ID_Prava as userRoleId
                    FROM OdjeliMe imePrava
                    left join Prava
                    on imePrava.ID_OdjeliMe  = Prava.IDOdjelMe
                    and Prava.IDUsertest = ' . $userId . '');

                return response(json_encode($roles), 200);
            }
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
}
