<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class InsertController extends Controller
{

    public static function InsertData($token, $file, $object, $externalId = '', $upsert = false)
    {
        if ($upsert) {
            $reqData = [
                'object' => $object,
                'contentType' => "CSV",
                "externalIdFieldName" => $externalId,
                'operation' => "upsert"
            ];
        } else {
            $reqData = [
                'object' => $object,
                'contentType' => "CSV",
                'operation' => "insert"
            ];
        }
        //open ingest job and get jobId
        $response = Http::withToken($token)->withHeaders(['Content-Type: application/json; charset=UTF-8', 'Accept: application/json'])
            ->post(env('SALESFORCE_URL') . '/services/data/v53.0/jobs/ingest', $reqData);
        error_log('after open job: ' . $response);

        if ($response['state'] == "Open") {

            $jobId = $response['id'];

            //ingest csv file
            $response = Http::withToken($token)->accept('application/json')
                ->withBody(file_get_contents($file), 'text/csv')
                ->put(
                    env('SALESFORCE_URL') . '/services/data/v53.0/jobs/ingest/' . $jobId . '/batches/'
                );
            error_log('after batch upload: ' . $response);

            //send upload complete and trigger batch job validation
            $response = Http::withToken($token)->withHeaders(['Content-Type: application/json; charset=UTF-8', 'Accept: application/json'])
                ->patch(env('SALESFORCE_URL') . '/services/data/v53.0/jobs/ingest/' . $jobId . '/', [
                    'state' => "UploadComplete"
                ]);
            error_log('after upload complete sent: ' . $response);

            while (true) {
                //get state of batch job
                $response = Http::withToken($token)->accept('application/json')
                    ->get(env('SALESFORCE_URL') . '/services/data/v53.0/jobs/ingest/' . $jobId);
                error_log('after get state: ' . $response);

                if ($response['state'] != 'InProgress' && $response['state'] != "UploadComplete") { //if done processing

                    $failedNumber = $response['numberRecordsFailed'];

                    //get failed/success results
                    $successful = Http::withToken($token)->accept('text/csv')
                        ->get(env('SALESFORCE_URL') . '/services/data/v53.0/jobs/ingest/' . $jobId . '/successfulResults');

                    $failed = Http::withToken($token)->accept('text/csv')
                        ->get(env('SALESFORCE_URL') . '/services/data/v53.0/jobs/ingest/' . $jobId . '/failedResults');

                    error_log('failed/successful results: ' . $response);

                    break;
                }

                usleep(300 * 1000); //300ms
            }
        }
        return [$failed, $successful, $failedNumber];
    }

    public function InsertBulk(Request $req)
    {
        try {
            //get Oauth2 token
            $res = Http::asForm()->post(env('SALESFORCE_URL') . '/services/oauth2/token', [
                'client_id' => env('SALESFORCE_CLIENT_ID'),
                'client_secret' => env('SALESFORCE_SECRET'),
                'username' => env('SALESFORCE_USERNAME'),
                'password' => env('SALESFORCE_PASSWORD'),
                'grant_type' => 'password'
            ]);

            error_log($res);
            $token = $res['access_token'];



            //get "distinct" users from database"
            $users = DB::select('SELECT *
                                    FROM [DEVELOPMENT].[dbo].[SF_UserId]
                                    where transfer is null or Transfer = 0');


            //prtepare owner users
            $investOwner = '0050900000ASFSD';
            $kreditOwner = '0050900000ASH1d';

            //prepare csv data and headers for accounts insert
            $file = fopen('accountsInvest.csv', 'w');
            fputcsv($file, [
                'ExtId__c',
                'Name',
                'RecordTypeId',
                'Mjesto__c',
                'Adresa__c',
                'OIB__c',
                'OwnerId'
            ]);
            foreach ($users as $key => $value) {
                $value->Ime = str_replace("\n", '', $value->Ime);
                $value->Prezime = str_replace("\n", '', $value->Prezime);
                $value->Ime = str_replace(" ", '', $value->Ime);
                $value->Prezime = str_replace(" ", '', $value->Prezime);

                if (($value->Ime == null || $value->Ime == '') && ($value->Prezime == null || $value->Prezime == '')) {
                    $value->Ime = 'Nema';
                    $value->Prezime = 'Podataka';
                    $value->ID = 'NemaPodataka';
                }
                $value->Mjesto = str_replace("\n", '', $value->Mjesto);
                $value->Adresa = str_replace("\n", '', $value->Adresa);

                fputcsv($file, [
                    $value->ID,
                    $value->Ime . ' ' . $value->Prezime,
                    $value->Izvor == 'INVEST' ? '01209000000afbuAAA' : ($value->Izvor == 'OTKUP' ? '01209000000afbzAAA' : '01209000000adFcAAI'),
                    $value->Mjesto,
                    $value->Adresa,
                    InsertController::CheckOIB($value->OIB) ? $value->OIB : 0,
                    $value->Izvor == 'INVEST' ? $investOwner : ($value->Izvor == 'KREDIT' ? $kreditOwner : ''),
                ]);
            }
            fclose($file);
            [$failedCSV, $successfulCSV, $failedNumber] = InsertController::InsertData($token, 'accountsInvest.csv', 'Account', 'ExtId__c', true); //inserts accounts
            error_log($failedCSV);

            //prepare csv for contacts insert
            $file = fopen('ContactsInvest.csv', 'w');
            fputcsv($file, [
                'Account.ExtId__c',
                'ExtId__c',
                'FirstName',
                'LastName',
                'Phone',
                'OtherPhone',
                'Email__c',
                'OwnerId'
            ]);
            foreach ($users as $key => $value) {
                fputcsv($file, [
                    $value->ID,
                    $value->ID,
                    $value->Ime == '' ? '-' : $value->Ime,
                    $value->Prezime == '' ? '-' : $value->Prezime,
                    $value->Telefon,
                    $value->Telefon2,
                    $value->Email,
                    $value->Izvor == 'INVEST' ? $investOwner : ($value->Izvor == 'KREDIT' ? $kreditOwner : '')
                ]);
            }

            fclose($file);

            [$failedCSV, $successfulCSV, $failedNumber] = InsertController::InsertData($token, 'ContactsInvest.csv', 'Contact', 'ExtId__c', true); //inserts


            $failed = InsertController::convertToArray($failedCSV);
            $successful = InsertController::convertToArray($successfulCSV);

            error_log(json_encode($failed));
            foreach ($successful as $key => $value) {
                if ($key == 0) continue;
                DB::select('Update development.dbo.sf_userid set Transfer = 1, TransferLog = \'' . str_replace("'", "", $value[0]) . '~' . Carbon::now() . '\'
                                where ID = ' . $value[3]);
            }
            foreach ($failed as $key => $value) {
                if ($key == 0) continue;
                DB::select('Update development.dbo.sf_userid set Transfer = 0, TransferLog = \'' . str_replace("'", "", $value[1]) . '\'
                                where ID = ' . $value[3]);
            }




            $cases = DB::select('SELECT [ID]
                                    ,[IDUserID]
                                    ,[RacBranch]
                                    ,[RacID]
                                    ,[RacType]
                                    ,CONVERT(varchar,RacDateTime,126) as RacDateTime
                                    ,[RacAmount]
                                    ,[RacAmount1]
                                    ,[RacAmount2]
                                    ,[RacAmount3]
                                    ,[Transfer]
                                    ,[Items]
                                FROM [DEVELOPMENT].[dbo].[SF_UserId_rac] where transfer = 0 or transfer is null');

            //prepare csv for "case" insert
            $file = fopen('CaseInvest.csv', 'w');
            fputcsv($file, [
                'Contact.ExtId__c',
                'ExtId__c',
                'TempId__c',
                'Kasa__c',
                'Nadnevak__c',
                'Iznossapdv__c',
                'Iznosbezpdv__c',
                'Items__c',
                'OwnerId'
            ]);

            foreach ($cases as $key => $value) {

                $IDAppend = $value->RacType == 'INVEST' ? 'I' : ($value->RacType == 'OTKUP' ? 'O' : 'C');

                fputcsv($file, [
                    $value->IDUserID,
                    $value->RacID . $IDAppend,
                    $value->ID,
                    $value->RacBranch,
                    $value->RacDateTime,
                    $value->RacAmount2,
                    $value->RacAmount,
                    $value->Items,
                    $value->RacType == 'INVEST' ? $investOwner : ($value->RacType == 'KREDIT' ? $kreditOwner : '')
                ]);
            }
            fclose($file);

            [$failedCSV, $successfulCSV, $failedNumber]  = InsertController::InsertData($token, 'CaseInvest.csv', 'Case', 'ExtId__c', true); //inserts

            $failed = InsertController::convertToArray($failedCSV);
            $successful = InsertController::convertToArray($successfulCSV);
            foreach ($successful as $key => $value) {
                if ($key == 0) continue;
                DB::select('Update development.dbo.sf_userid_rac set Transfer = 1, TransferLog = \'' . $value[0] . '~' . Carbon::now() . '\'
                                where ID = ' . $value[4]);
            }
            foreach ($failed as $key => $value) {
                if ($key == 0) continue;
                DB::select('Update development.dbo.sf_userid_rac set Transfer = 0, TransferLog = \'' . $value[1] . '\'
                                where ID = ' . $value[4]);
            }


            return response($failedCSV);
        } catch (\Exception $e) {
            // if ($file) {
            //     fclose($file);
            // }
            throw $e;
        }
    }

    public static function CheckOIB($oib)
    {
        if (mb_strlen($oib) != 11 || (!is_numeric($oib))) {
            return false;
        }
        $ostatak = 10;
        for ($i = 0; $i < 10; $i++) {
            $trenutnaZnamenka = (int) $oib[$i];
            $zbroj = $trenutnaZnamenka + $ostatak;
            $meduOstatak = $zbroj % 10;
            if ($meduOstatak == 0) {
                $meduOstatak = 10;
            }
            $umnozak = $meduOstatak * 2;
            $ostatak = $umnozak % 11;
        }
        if ($ostatak == 1) {
            $kontrolnaZnamenka = 0;
        } else {
            $kontrolnaZnamenka = 11 - $ostatak;
        }
        if (((int) $oib[10]) == $kontrolnaZnamenka) {
            return true;
        }
        return false;
    }

    static function convertToArray(string $content): array
    {
        $data = str_getcsv($content, "\n");
        array_walk($data, function (&$a) use ($data) {
            $a = str_getcsv($a);
        });

        return $data;
    }
}
