<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class AccountController extends Controller
{
    function InsertSingle(Request $req){
        AccountController::InsertSingleUserAndCasesAssoc($req->input('ID'));
    }

    function InsertMultiple(Request $req){



        //AccountController::InsertSingleUserAndCasesAssoc($req->input('ID'));
    }

    private static function InsertSingleUserAndCasesAssoc($userId)
    {
        try {


            $user = DB::select('select * from development.dbo.SF_userid where ID = ' . $userId);


            $token = Http::asForm()->post(env('SALESFORCE_URL') . '/services/oauth2/token', [
                'client_id' => '3MVG9t0sl2P.pByoUxTUrcmPuOYtzyD6GOxPlVe457onlj0DIItLwgrOwx5ZLZaF_sYjwohS5jPKFd3dQM7aW',
                'client_secret' => '847B6BA2E65579995C9BE92D2393D32C44E67F11C7206D70D30AFAD312ED4C41',
                'username' => env('SALESFORCE_USERNAME'),
                'password' => env('SALESFORCE_PASSWORD'),
                'grant_type' => 'password'
            ])['access_token'];


            error_log($user[0]->Ime . ' ' . $user[0]->Prezime . '    ' . $user[0]->ID);
            $res = Http::withToken($token)->withBody(json_encode([
                'Name' => $user[0]->Ime . ' ' . $user[0]->Prezime,
                'RecordTypeId' => $user[0]->Izvor == 'INVEST' ? '0127Q000000TpCwQAK' : ($user[0]->Izvor == 'OTKUP' ? '0127Q000000TpCXQA0' : '0127Q000000TpCrQAK'),
                'Mjesto__c' => $user[0]->Mjesto,
                'Adresa__c' => $user[0]->Adresa,
                'OIB__c' => $user[0]->OIB
            ]), 'application/json')->patch(env('SALESFORCE_URL') . '/services/data/v53.0/sobjects/Account/ExtId__c/' . $userId);


            $res = Http::withToken($token)->withBody(json_encode([
                'Account' => ['ExtId__c' => $userId],
                'FirstName' => $user[0]->Ime,
                'LastName' => $user[0]->Prezime,
                'Phone' => $user[0]->Telefon,
                'OtherPhone' => $user[0]->Telefon2,
                'Email__c' => $user[0]->Email
            ]), 'application/json')->patch(env('SALESFORCE_URL') . '/services/data/v53.0/sobjects/Contact/ExtId__c/' . $userId);

            error_log('log: ' . $res);

            if (isset($res['success']) && $res['success'] == true) {
                DB::select('update development.dbo.SF_userid set TransferLog = \'\', Transfer = 1 where ID = ' . $userId);
            } else {
                DB::select('update development.dbo.SF_userid set TransferLog = \'' . $res . '\', Transfer = 0 where ID = ' . $userId);
            }

            $racuni =   DB::select('SELECT [ID]
                                        ,[IDUserID]
                                        ,[RacBranch]
                                        ,[RacID]
                                        ,[RacType]
                                        ,CONVERT(varchar,RacDateTime,126) as RacDateTime
                                        ,[RacAmount]
                                        ,[RacAmount1]
                                        ,[RacAmount2]
                                        ,[RacAmount3]
                                        ,[Transfer]
                                    FROM [DEVELOPMENT].[dbo].[SF_UserId_rac] where (transfer = 0 or transfer is null)
                                    and IDUserID = ' . $userId);

            foreach ($racuni as $key => $value) {

                $IDAppend = $value->RacType == 'INVEST' ? 'I' : ($value->RacType == 'OTKUP' ? 'O' : 'C');

                $res = Http::withToken($token)->withBody(json_encode([
                    'Contact' => ['ExtId__c' => $userId],
                    'TempId__c' => $value->ID,
                    'Kasa__c' => $value->RacBranch,
                    'Nadnevak__c' => $value->RacDateTime,
                    'Iznossapdv__c' => $value->RacAmount2,
                    'Iznosbezpdv__c' => $value->RacAmount,
                ]), 'application/json')->patch(env('SALESFORCE_URL') . '/services/data/v53.0/sobjects/Case/ExtId__c/' . $value->RacID . $IDAppend);

                if (isset($res['success']) && $res['success'] == true) {
                    DB::select('update [DEVELOPMENT].[dbo].[SF_UserId_rac] set transfer = 1, transferlog = \'' . $res['id'] . '~' . Carbon::now() . '\'
                                    where ID = ' . $value->ID);
                } else {
                    DB::select('update [DEVELOPMENT].[dbo].[SF_UserId_rac] set transfer = 0, transferlog = \'' . $res . '\'
                                    where ID = ' . $value->ID);
                }
            }




            return response($res);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
