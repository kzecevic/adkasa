<?php

namespace App\Http\Controllers;

use App\Models\Fridge;
use App\Models\IcecreamPot;
use App\Models\IcecreamPotWeightHistory;
use Carbon\Carbon;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScalesController extends Controller
{
    function InsertData(Request $req)
    {
        try {
            $input = $req->collect();
            $fakeMainEspId = 1337;


            foreach ($input as $key => $value) {
                $icecreamPot = IcecreamPot::firstOrCreate(
                    ['esp_id' => $key, 'value' => true],
                    ['flavor' => null, 'fridge_id' => 5, 'created:_at' => Carbon::now()->addHours(2)]
                );
                $potWeightHistory = IcecreamPotWeightHistory::create([
                    'pot_id' => $icecreamPot->id,
                    'weight' => $value,
                    'created_at' => Carbon::now()->addHours(2)
                ]);
            }
            error_log("insert data finished");
            return response('asdasd');
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
    function InsertDada(Request $req)
    {
        try {
            $input = $req->collect();
            error_log($req);
            // if (gettype($input["vals"] == "array")) {

            //     $fridge = Fridge::firstOrCreate(["fridge_adress" => $input["frigde"]], ["name" => $input["frigde"]]);
            //     $pot = IcecreamPot::firstOrCreate(["esp_id" => $input["node"]], ["fridge_id" => $fridge["id"], "value" => true]);
            //     error_log(json_encode($fridge));
            //     $vals = $input["vals"];


            //     foreach ($vals as $key => $value) {
            //         IcecreamPotWeightHistory::create(["pot_id" => $pot["id"], "weight" => $value, 'created_at' => Carbon::now()->addHours(2)]);
            //     }

            // foreach ($input as $key => $value) {
            //     $icecreamPot = IcecreamPot::firstOrCreate(
            //         ['esp_id' => $key, 'value' => true],
            //         ['flavor' => null, 'fridge_id' => 5, 'created:_at' => Carbon::now()->addHours(2)]
            //     );
            //     $potWeightHistory = IcecreamPotWeightHistory::create([
            //         'pot_id' => $icecreamPot->id,
            //         'weight' => $value,
            //         'created:_at' => Carbon::now()->addHours(2)
            //     ]);
            // }
            //}
            return response('asdasd');
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }


    function GetData(Request $req)
    {
        try {
            $input = $req->collect();
            error_log($input);
            if ($input["first"] == "true") {
                error_log("first req; data: " . json_encode($input));
                return inertia('test', [
                    'potsData' => null,
                    'fridges' => Fridge::all(),
                    'potsHistoryData' => null,
                    'fridgePots' => null
                ]);
            } else {
                error_log("NON 1st req,data: " . json_encode($input));
                // $test = IcecreamPot::where('fridge_id', $input["fridge_id"])->get();
                // error_log(json_encode($test));
                // $test = isset($input["fridge_id"]) ? IcecreamPot::where("fridge_id", $input["fridge_id"])->get(): null;
                // error_log(json_encode($test));
                error_log(Carbon::now());

                $datetime = isset($input["datetime"]) ? Carbon::parse($input["datetime"])->addHours(2) : Carbon::now()->addHours(2);
                $datetimeOd = isset($input["datetimeOd"]) ? Carbon::parse($input["datetimeOd"])->addHours(2) : Carbon::createFromDate(1975, 5, 21);
                error_log("fetching current data");

                $currentData = isset($input["fridge_id"]) ? DB::select(DB::raw("WITH MostRecentRows AS(
                    SELECT  weight,pot_id,created_at,
                    ROW_NUMBER() OVER (PARTITION BY pot_id ORDER BY created_at desc) AS 'RowNumber'
                    FROM icecream_pot_weight_history
                    )
                    SELECT weight,pot_id,flavor,fridge_id, CONVERT(varchar,l.created_at,13) as created_at FROM MostRecentRows l
                    JOIN icecream_pot r on l.pot_id=r.id
                    where fridge_id = " . $input["fridge_id"] . " and RowNumber = 1")) : null;
                error_log("fetching history data");
                $potsHistoryData = isset($input["flavors"]) ?  IcecreamPotWeightHistory::whereIn("pot_id", $input["flavors"])->where(
                    "created_at",
                    "<",
                    $datetime
                )->where(
                    "created_at",
                    ">",
                    $datetimeOd
                )->get() : null;
                error_log("returning");
                return inertia('test', [
                    'potsData' => isset($input["fridge_id"]) ? IcecreamPot::where('fridge_id', $input["fridge_id"])->get() : null,
                    'fridges' => Fridge::all(),
                    'potsHistoryData' => $potsHistoryData,
                    'espPots' => isset($input["esp_id"]) ?  IcecreamPot::where("esp_id", $input["esp_id"])->get() : null,
                    'fridgePots' => isset($input["fridge_id"]) ? IcecreamPot::where("fridge_id", $input["fridge_id"])->get() : null,
                    'currentData' => $currentData
                ]);
            }
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }

    function UpdateFridgePots(Request $req)
    {
        try {
            $input = $req->collect();

            error_log($input);
            $pots = $input["fridgePots"];
            foreach ($pots as $value) {
                error_log($input);
                IcecreamPot::where("id", $value["id"])->update([
                    "flavor" => $value["flavor"]
                ]);
            }
            return response("success");
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
}
