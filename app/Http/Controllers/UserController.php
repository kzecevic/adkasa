<?php

namespace App\Http\Controllers;

use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function GetUsers(Request $req)
    {
        try {
            if ($req->session()->get('authorized', false)) {

                $users = DB::table('usertest')
                    ->join('operateri', 'usertest.IdOperateri', '=', 'operateri.ID_Operateri')
                    ->select(DB::raw(
                        'usertest.ID_usertest as idUsertest,
                        usertest.IdOperateri,
                        operateri.Imeprezime as username,
                        operateri.imeRacun,
                        usertest.sifra as password,
                        FORMAT (usertest.DatumZadnjegLogina, \'dd/MM/yyyy\') as lastLogin,
                        operateri.OIB as OIB'
                    ))
                    ->where('usertest.DatumZadnjegLogina', '>=', Carbon::now()->subDays(365)->toDateString())
                    ->get();
                //$users->lastLogin->format('d-m-Y');

                //$users = array_values($users->toArray());
                return response(json_encode($users), 200);
            }
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }

    public function NewPassword(Request $req)
    {
        try {
            if ($req->session()->get('authorized', false)) {

                $idUserTest = $req->input('idUsertest');
                $idOperater = $req->input('idOperater');


                $sifra = DB::select('exec dbo.KREIRAJ_SIFRU @KojiIdUserTest = ' . $idUserTest . ' , @KojiIdOperateri = ' . $idOperater);
                $sifra = DB::table('usertest')->select('Sifra')->where('ID_usertest', '=', $idUserTest)->first();

                return response(json_encode($sifra), 200);
            }
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }

    public function Add(Request $req)
    {
        try {
            if ($req->session()->get('authorized', false)) {

                if ($req->input('username') == '')
                    return response('Morate unjeti ime korisnika', 204);

                $idOperateri = DB::table('operateri')->insertGetId(
                    ['Imeprezime' => $req->input('username'), 'OIB' => $req->input(('OIB')), 'ImeRacun' => $req->input('imeRacun')]
                );
                $replaced = Str::replace(' ', '.', $req->input('username'));
                $idUserTest = DB::table('usertest')->insertGetId(
                    ['Imeprezime' => $replaced, 'DatumZadnjeglogina' => Carbon::now()->toDateString(), 'IdOperateri' => $idOperateri]
                );
                $sifra = DB::select('exec dbo.KREIRAJ_SIFRU @KojiIdUserTest = ' . $idUserTest . ' , @KojiIdOperateri = ' . $idOperateri . '');
                error_log(json_encode($sifra));
                return response(['Dodan novi korisnik: ' . $req->input('username'), $idUserTest], 200);
            }
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }

    public function EditUser(Request $req)
    {
        try {
            if ($req->session()->get('authorized', false)) {

                if ($req->input('username') == '')
                    return response('Morate unjeti ime korisnika', 204);

                $idUsertest = $req->input('idUsertest');
                $idOperateri = $req->input('idOperater');
                $OIB = $req->input('OIB');
                $imeRacun = $req->input('imeRacun');
                $username = $req->input('username');
                $replaced = Str::replace(' ', '.', $username);

                error_log($idUsertest . ' ' . $idOperateri);

                DB::table('operateri')->where('ID_operateri', '=', $idOperateri)->update([
                    'Imeprezime' => $username,
                    'OIB' => $OIB,
                    'ImeRacun' => $imeRacun
                ]);

                DB::table('usertest')->where('ID_usertest', '=', $idUsertest)->update([
                    'Imeprezime' => $replaced
                ]);

                return response('Izmjenjen korisnik: ' . $req->input('username'), 200);
            }
        } catch (\Exception $e) {
            error_log($e);
            throw ($e);
        }
    }
}
