<?php

namespace App\Http\Middleware;
use Closure;


class Authenticate
{
    public function handle($request, Closure $next)
    {
        if ($request->session()->get('authorized', false)) {
            return $next($request);
        }
        else{
            return redirect("login");
        }
    }
}
