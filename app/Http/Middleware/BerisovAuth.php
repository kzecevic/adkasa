<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BerisovAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->get('authorizedAroma', false)) {
            return $next($request);
        } else {
            return redirect("aroma/login");
        }
    }
}
