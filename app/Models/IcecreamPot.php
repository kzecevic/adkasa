<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IcecreamPot extends Model
{
    use HasFactory;
    protected $table = 'icecream_pot';
    protected $primaryKey = 'id';
    protected $fillable = ['esp_id', 'value', 'fridge_id'];

    public function icecream_pot_weight_history()
    {
        return $this->hasMany(IcecreamPotWeightHistory::class, 'pot_id');
    }
}
