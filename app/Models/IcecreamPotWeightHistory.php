<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IcecreamPotWeightHistory extends Model
{
    use HasFactory;
    protected $fillable = ['pot_id', 'weight'];
    protected $table = 'icecream_pot_weight_history';

    protected $casts = [
        'created_at' => 'datetime:d.m.Y H:i:s',
         'updated_at' => 'datetime:d.m.Y H:i:s',
    ];

    public function icecream_pot()
    {
        return $this->belongsTo(IcecreamPot::class, 'pot_id');
    }
}
