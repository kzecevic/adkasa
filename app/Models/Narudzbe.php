<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Narudzbe extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'WEB_narudzbe';
    protected $connection = 'aroma';
    const CREATED_AT = 'Nadnevak';
    const UPDATED_AT = 'NadnevakUpdate';
    protected $casts = [
        'Nadnevak' => 'datetime:d-m-Y H:i:s',
        'NadnevakUpdate' => 'datetime:d-m-Y H:i:s',
        'NadnevakZaDostavu' => 'datetime:d-m-Y H:i:s',
        'Nadnevak_Dostave' => 'datetime:d-m-Y H:i:s',
        'NadnevakZakljucano' => 'datetime:d-m-Y H:i:s',
        'NadnevakDostava' => 'datetime:d-m-Y H:i:s',
        'NadnevakZakljucanoProizvodnja' => 'datetime:d-m-Y H:i:s',
        'NadnevakProizvodnje' => 'datetime:d-m-Y H:i:s',
    ];
    public function kupac()
    {
        return $this->hasOne(Kupci::class, 'ID', 'IDWEBKUP');
    }
}
