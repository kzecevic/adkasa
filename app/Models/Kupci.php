<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kupci extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'WEB_kup';
    protected $connection = 'aroma';
    const CREATED_AT = 'NadnevakUpdate';
    const UPDATED_AT = 'NadnevakUpdate';
}
