<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stavke extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'WEB_narudzbe_STAVKE';
    protected $connection = 'aroma';
    const CREATED_AT = 'NadnevakUpdate';
    const UPDATED_AT = 'NadnevakUpdate';
    public function roba()
    {
        return $this->hasOne(Roba::class, 'ID', 'IDWEB_ROBE');
    }
}
