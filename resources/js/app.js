import Vue from "vue";
import { createInertiaApp } from "@inertiajs/inertia-vue";
import { InertiaProgress } from "@inertiajs/progress";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import VueApexCharts from "vue-apexcharts";
import ScaleSettingsModal from "./Pages/ScaleSettingsModal.vue";
import DatetimePicker from "vuetify-datetime-picker";
import HistoryDataTable from "./Pages/HistoryDataTable.vue";
import CurrentStateDataTable from "./Pages/CurrentStateDataTable.vue";

Vue.use(DatetimePicker);
Vue.use(Vuetify);
Vue.use(VueApexCharts);
Vue.component("apexchart", VueApexCharts);
Vue.component("scale-settings-modal", ScaleSettingsModal);
Vue.component("history-data-table", HistoryDataTable);
Vue.component("current-state-data-table", CurrentStateDataTable);

InertiaProgress.init();

createInertiaApp({
    resolve: (name) => require(`./Pages/${name}`),
    setup({ el, App, props }) {
        new Vue({
            vuetify: new Vuetify(),
            render: (h) => h(App, props),
        }).$mount(el);
    },
});
