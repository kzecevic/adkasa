<x-html-head></x-html-head>

<body>
    <div id="app">
        <v-app id="inspire">
            <v-alert style='position: fixed; left: 50%; bottom: 50px; transform: translate(-50%, -50%); margin: 0 auto;'
                :value="alertShow" elevation="8" :type="alertType" transition="scale-transition" shaped dense width=300>
                @{{ alertMessage }}
            </v-alert>

            <v-data-table class="scroll-y" :dense="dense" :dark="dark" height="94vh" :headers="headers"
                :items="users" sort-desc sort-by="lastLogin" disable-pagination hide-default-footer fixed-header
                :search="search" :loading="loading" class="elevation-1"
                @click:row="(item, slot) => rowClick(item, slot)" item-key="IdUsertest" {{-- :footer-props="{
                    showFirstLastPage: true,
                    firstIcon: 'mdi-arrow-collapse-left',
                    lastIcon: 'mdi-arrow-collapse-right',
                    prevIcon: 'mdi-minus',
                    nextIcon: 'mdi-plus',
                    itemsPerPageOptions: [15, 20, 25, 30, 35, -1],
                  }" --}}>



                <template v-slot:top v-slot:activator="{ on}">
                    <v-toolbar flat dark id="scroll-target">
                        <v-img src='ad_logo_white.png' max-height="50"
                            max-width="100"></v-img>
                        <v-divider class="mx-4" inset vertical></v-divider>
                        <v-toolbar-title>Korisnička prava</v-toolbar-title>
                        <v-divider inset vertical class="ml-5 mr-3"></v-divider>
                        <label> {{ session()->get('username') }} </label>

                        <v-divider inset vertical class="ml-5 mr-3"></v-divider>
                        <v-card-title>
                            {{-- <v-text-field v-model="search" append-icon="mdi-magnify" label="Pretraga po imenu"
                                hide-details></v-text-field> --}}
                            <v-text-field :value="search" @change="v => search = v" append-icon="mdi-magnify"
                                label="Pretraga po imenu" hide-details></v-text-field>
                        </v-card-title>
                        <input class="mr-1" type="checkbox" v-model="dark" v-on:change="darkDenseChanged">
                        <label>Mračni način</label>
                        <input class="ml-2 mr-1" type="checkbox" class="ml-5" v-model="dense"
                            v-on:change="darkDenseChanged">
                        <label>Kompaktna tablica</label>
                        <v-spacer></v-spacer>




                        <v-btn color="primary" dark class="mb-2" v-on:click="openAddUser">
                            <v-icon class="mr-2">
                                mdi-account-multiple-plus
                            </v-icon>
                            Novi korisnik
                        </v-btn>
                        <v-divider inset vertical class="ml-2 mr-2"></v-divider>
                        <v-btn color="primary" dark class="mb-2" v-on:click="openAddRoles">
                            <v-icon class="mr-2">
                                mdi-playlist-plus
                            </v-icon>
                            Nova uloga/pravo
                        </v-btn>
                        <v-divider inset vertical class="ml-2 mr-2"></v-divider>
                        <v-btn color="secondary" dark class="mb-2" v-on:click="logout">
                            <v-icon class="mr-2">
                                mdi-logout
                            </v-icon>
                            Odjava
                        </v-btn>

                    </v-toolbar>

                    {{-- <v-btn v-scroll="onScroll" v-show="fab" fab dark fixed bottom right color="primary"
                            @click="toTop">
                            <v-icon>mdi-arrow-up-bold</v-icon>
                        </v-btn> --}}

                    {{-- New user dialog --}}
                    <v-dialog v-model="dialog" max-width="500px">
                        {{-- <template v-slot:activator="{ on, attrs }">
                            <v-btn color="primary" dark class="mb-2" v-bind="attrs" v-on:click="openAddUser">
                                Novi korisnik
                            </v-btn>
                            <v-divider inset vertical class="ml-2 mr-2"></v-divider>
                            <v-btn color="primary" dark class="mb-2" v-on:click="openAddRoles">
                                Nova uloga/pravo
                            </v-btn>
                        </template> --}}
                        <v-card>
                            <v-card-title>
                                <span class="text-h5">Novi korisnik</span>
                            </v-card-title>
                            <v-card-text>
                                <v-container>
                                    <v-row>
                                        <v-col cols="12" sm="6" md="6">
                                            <v-text-field :value="editedItem.username"
                                                @change="v => editedItem.username = v" label="Ime i prezime">
                                            </v-text-field>
                                        </v-col>

                                        <v-col cols="12" sm="6" md="6">
                                            <v-text-field :value="editedItem.imeRacun"
                                                @change="v => editedItem.imeRacun = v" label="Ime na računu">
                                            </v-text-field>
                                        </v-col>
                                        <v-col cols="12" sm="6" md="6">
                                            {{-- <v-text-field v-model="editedItem.OIB" label="OIB"></v-text-field> --}}
                                            <v-text-field :value="editedItem.OIB" @change="v => editedItem.OIB = v"
                                                label="OIB"></v-text-field>

                                        </v-col>
                                    </v-row>
                                </v-container>
                            </v-card-text>

                            <v-card-actions>
                                <v-spacer></v-spacer>
                                <v-btn color="red darken-1" text @click="close">
                                    Odustani
                                </v-btn>
                                <v-btn color="blue darken-1" text @click="save">
                                    Spremi
                                </v-btn>
                            </v-card-actions>
                        </v-card>
                    </v-dialog>


                    {{-- Edit user dialog --}}
                    <v-dialog v-model="dialogEditUser" max-width="500px">

                        <v-card>
                            <v-card-title>
                                <span class="text-h5">Izmjena korisničkih podataka</span>
                            </v-card-title>
                            <v-card-text>
                                <v-container>
                                    <v-row>
                                        <v-col cols="12" sm="6" md="6">
                                            <v-text-field :value="editedItem.username"
                                                @change="v => editedItem.username = v" label="Ime i prezime">
                                            </v-text-field>
                                        </v-col>

                                        <v-col cols="12" sm="6" md="6">
                                            <v-text-field :value="editedItem.imeRacun"
                                                @change="v => editedItem.imeRacun = v" label="Ime na računu">
                                            </v-text-field>
                                        </v-col>
                                        <v-col cols="12" sm="6" md="6">
                                            <v-text-field :value="editedItem.OIB" @change="v => editedItem.OIB = v"
                                                label="OIB"></v-text-field>
                                        </v-col>
                                        <v-col cols="12" sm="6" md="6">
                                            <v-text-field :value="tmpPassword" @change="v => tmpPassword = v"
                                                label="Šifra"> </v-text-field>
                                        </v-col>

                                    </v-row>
                                </v-container>
                            </v-card-text>

                            <v-card-actions>
                                <v-spacer></v-spacer>
                                <v-btn color="grey darken-1" text @click="generateNewPassword">
                                    Generiraj novu šifru
                                </v-btn>
                                <v-btn color="red darken-1" text @click="closeEditUser">
                                    Odustani
                                </v-btn>
                                <v-btn color="blue darken-1" text @click="saveEditUser">
                                    Spremi
                                </v-btn>

                            </v-card-actions>
                        </v-card>
                    </v-dialog>

                    {{-- Add roles dialog --}}

                    <v-dialog v-model="dialogAddRole" max-width="600px">
                        <v-card>
                            <v-card-title text-center>
                                <span class="text-h5">Dodavanje uloge/prava</span>
                            </v-card-title>
                            <v-container class="pa-10">
                                <v-row>
                                    <v-col cols="12" sm="12" md="12">
                                        <v-text-field :value="newRole" @change="v => newRole = v"
                                            label="Naziv uloge/prava">
                                        </v-text-field>
                                    </v-col>

                                </v-row>
                            </v-container>
                            <v-card-actions>
                                <v-spacer></v-spacer>
                                <v-btn color="red darken-1" text @click="closeAddRoles">
                                    Odustani
                                </v-btn>
                                <v-btn color="blue darken-1" text @click="saveAddRoles">
                                    Spremi
                                </v-btn>
                            </v-card-actions>
                        </v-card>
                    </v-dialog>

                    {{-- User roles dialog --}}


                    <v-dialog v-model="dialogRoles" max-width="700px">
                        <v-card>
                            <v-card-title text-center>
                                <span class="text-h5">Izmjena uloga/prava</span>
                            </v-card-title>
                            <hr>
                            <v-container fill-height class="pa-10">
                                <v-row>
                                    <v-col align-self="center" v-for="(item, index) in editedItem.roles"
                                        v-bind:key="index" cols="12" xs="12" sm="6" md="4">
                                        <label>
                                            <input type="checkbox" v-model="editedItem.roles[index].userId">
                                            @{{ item . roleName }}
                                        </label>
                                    </v-col>

                                    {{-- <v-col v-for="(item, index) in editedItem.roles">

                                            <v-checkbox dense v-bind:key="index"
                                                v-model="editedItem.roles[index].userId" :label="item.roleName">
                                        </v-col> --}}
                                </v-row>
                            </v-container>
                            <v-card-actions>
                                <v-spacer></v-spacer>
                                <v-btn color="red darken-1" text @click="closeRoles">
                                    Odustani
                                </v-btn>
                                <v-btn color="blue darken-1" text @click="saveRoles">
                                    Spremi
                                </v-btn>
                            </v-card-actions>
                        </v-card>
                    </v-dialog>
                    {{-- </v-toolbar> --}}
                </template>

                <template v-slot:item.actions="{ item }">
                    <v-icon small class="mr-2" @click="editRoles(item)">
                        mdi-key-chain-variant
                    </v-icon>
                    <v-icon small class="mr-2" @click="editUser(item)">
                        mdi-account-cog
                    </v-icon>
                    {{-- <v-icon
                small
                @click="deleteItem(item)"
              >
                mdi-delete
              </v-icon> --}}
                </template>
                <template v-slot:no-data>
                    <v-btn color="primary" @click="initialize">
                        Reset
                    </v-btn>
                </template>
            </v-data-table>
        </v-app>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
    <script src="dataTable.js"></script>
</body>
