<x-html-head />

<body>
    <div id="app">
        <v-app id="inspire">
            <v-card width='500' class="mt-10 pa-10 mx-auto text-center">

                <img src='https://aurodomus.hr/wp-content/uploads/2019/11/aurodomus_logo_naslovna.png' max-height="150"
                    max-width="200" class="ml-150">

                <v-form ref="form" v-model="valid" lazy-validation class="mt-20" @submit="validate"
                    @submit.prevent>
                    <v-text-field v-model="name" :rules="nameRules" label="Korisnik" required></v-text-field>
                    <v-text-field type="password" v-model="password" :rules="passwordRules" label="Šifra" required>
                    </v-text-field>
                    <v-btn type="submit" :disabled="!valid" color="success" class="mr-4 mt-10"> Prijava </v-btn>
                </v-form>
            </v-card>
            <v-alert style='position: fixed; left: 50%; bottom: 50px; transform: translate(-50%, -50%); margin: 0 auto;'
                :value="alertShow" elevation="8" :type="alertType" transition="scale-transition" shaped dense width=300>
                @{{ alertMessage }}
            </v-alert>
        </v-app>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
    <script src="aromaLogin.js"></script>
    <script>
    </script>
</body>
