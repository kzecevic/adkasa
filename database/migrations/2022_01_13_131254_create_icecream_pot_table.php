<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcecreamPotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icecream_pot', function (Blueprint $table) {
            $table->increments("id");
            $table->timestamps();
            $table->string('esp_id');
            $table->boolean("value");
            $table->string("flavor")->nullable(true);
            $table->integer("fridge_id");
            $table->index("id");
            $table->index("esp_id");
            $table->unique(["esp_id", "value"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icecream_pot');
    }
}
