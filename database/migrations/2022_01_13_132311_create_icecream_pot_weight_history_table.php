<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcecreamPotWeightHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icecream_pot_weight_history', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->decimal("weight");
            $table->integer("pot_id");
            $table->foreign("pot_id")->references("id")->on("icecream_pot");
            $table->index("pot_id");
            $table->index("created_at");
            $table->index("id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icecream_pot_weight_history');
    }
}
